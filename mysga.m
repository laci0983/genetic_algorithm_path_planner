function fpop=mysga(initpop,maxgen,objfun,ggap,mutpr,rrate)
% A f�ggv�ny a genetikus algoritmus keretprogramja, mely megval�s�tja az optimumkeres�st.
% @param:
%           initpop - initial population
%           maxgen  -  maximal number of generations
%           objfun  - name of the objective function
%           ggap   - generation gap
%           mutpr -  probability of mutation
%           rrate  - reinsertion rate of offsprings
% @return: 
%          fpop  -  final population after optimization

global genNum;


population=initpop;
for i=1:maxgen
    genNum
    objvals=feval(objfun,population);
    [objvals,index]=sort(objvals);
    population=population(index,:);
    fvals=fitness(objvals);
    selected=select(fvals,ggap);
    offspring=xover(population,selected);
    objvals_f=feval(objfun,offspring);
    [objvals_f,index]=sort(objvals_f);
    offspring=mutate(offspring,mutpr);
    offspring=offspring(index,:);
    population=reinsert(population,offspring,floor(size(population,1)*(1-rrate)));
    genNum=genNum+1;
end
fpop=population;


