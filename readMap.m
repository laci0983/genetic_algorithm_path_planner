function [parkingPlaces,mapOfParkingLot] = readMap(filaName)
%This function parses the binary image of of map of the parking lot and
%the binary image of the parking zones
%@param:
%        fileName     - string describing the mat file name of the binary
%                       images storing binary of map and parking zones
%@return:
%        parkingPlaces - binary image of parking places
%        mapOfParkingLot  - binary image of road surface

global draw;
%%loading the map
load(filaName);
%%getting the parking places on the map
parkingPlaces=~(og~=2);

%%storing only the road parts
og(og~=1)=0;
if draw==1
    figure();
    imshow(~og);
end

image=og;

%%creating a binary image from map for image processing
mapOfParkingLot=im2bw(image);

end

