function objvals=objfn(pop)
%Funciton assigns objective funtion values to all individual,
%Objective funtion is the path needed for the traversal of the graph
%@param:
%       pop     -  population matrix size Nind x Nvar
%@output:
%       objval  -  objective funtion values for each individual (vector) 
%                   size: 1xNind

global startPoint;
global toggle;

%For Graphs
global G;

global trendOfError;

Nind=size(pop,1);
objvals=zeros(Nind,1); %All objvals are initialized as infite

%For Graphs
%d = distances(G);

for i=1:Nind
    pointsToReachInSequence=pop(i,:)';
    visited=[];
    for j=1:size(pointsToReachInSequence,1)-1  
        start=pointsToReachInSequence(j,:);
        pointToReach=pointsToReachInSequence(j+1,:);
        [I,~]=find(visited==pointToReach);
        if(length(I)<1)         

            [points,sectionLength]=shortestpath(G,start,pointToReach);
            if(length(points)>2)
                visited=[visited,points(1:end-1)];
            end
            if(length(visited)>0 & pointToReach==visited(end))%penalize reversal
                sectionLength=100*sectionLength;
            end
            objvals(i)= objvals(i)+sectionLength;   %length is is objective function value
            visited(end+1)=pointToReach;
        end
    end
    [~,sectionLength]=shortestpath(G,startPoint,pointsToReachInSequence(1,:));
    objvals(i)= objvals(i)+sectionLength;   %length is is objective function value 
    [~,sectionLength]=shortestpath(G,pointsToReachInSequence(end,:),startPoint);
     objvals(i)= objvals(i)+sectionLength;   %length is is objective function value 
end


minimalDist=min(objvals);
if(toggle==1)
    trendOfError=[trendOfError; minimalDist];
    toggle=0;
else
    toggle=1;
end

end

