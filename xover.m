function offspring=xover(pop,selected)
% Function performs single point xover on "binary" population
% where the selected individuals are the Parents (P1, P2). Each pair will
% have two offsprings
%@param:
%       pop -  population matrix size: Nind x Nvar
%       selected - number (index) of the selected individuals, 
%                   size: 1 x Nselect
%@output:
%       offspring -  offsrping matrix,
%                    size: Noff x Nvar

Nselect=length(selected);
Nvar=size(pop,2);

%Only even number of individuals should be selected
if(mod(Nselect,2)~=0)
   Nselect=Nselect-1;
end
%mixing the parents
selctedIndexes=randperm(Nselect);
selected=selected(selctedIndexes);


P1_ind=selected(1:Nselect/2);
P2_ind=selected((Nselect/2)+1:end);

P1=pop(P1_ind,:);
P2=pop(P2_ind,:);


Npairs=size(P1,1);


pointOfXover=floor(1+rand(Npairs,1)*(Nvar-1));

O1=[];
O2=[];

for i=1:Npairs
      idx=randperm(Nvar);
      O1=[O1;P1(i,idx)];
      O2=[O2;P2(i,idx)];
    

end

offspring=[O1;O2];

end

