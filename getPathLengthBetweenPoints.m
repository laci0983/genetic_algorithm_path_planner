function path_length= getPathLengthBetweenPoints(skeleton,start,pointToReach)

    %%finding the shortest path between 2 points
    D1 = bwdistgeodesic(skeleton, start(1), start(2), 'quasi-euclidean');
    D2 = bwdistgeodesic(skeleton, pointToReach(1), pointToReach(2), 'quasi-euclidean');
    D = D1 + D2;
    D = round(D * 8) / 8;
    
    D(isnan(D)) = inf;
    skeleton_path = imregionalmin(D);
    path_length_arr = D(skeleton_path);
    path_length=path_length_arr(1);
end

