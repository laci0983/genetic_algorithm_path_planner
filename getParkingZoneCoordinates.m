function [parkingZoneCoordinatesInSequence,centerPointsOfZones] = getParkingZoneCoordinates(parkingPlaces,skeletonOfParkingLot,startPoint)
% This function provides the sequence of points of the Voronoi diagram that
% are the closest points to the parking zone centroids. The function
% performs the planning of the traversal based on airline based distance
% minimization.
%@param:
%           parkingPlaces  - binary image of the parking zone
%                            representation
%           skeletonOfParkingLot - skeleton or Voronoi diagram of the
%                                  road surface aroung the parking lot
%           startPoint - coordinates of the initial configuration          
%                        Size: 1x2 Vector
%@return:
%           voronoiPointsInSequence - the sequence Voronoi diagram points
%                                     (coordinates) that are the closest 
%                                     point to the parking lots. The first 
%                                     element is the start point
%                                     Size:(N+1)x2
%           centerPointsOfZones  - centroids of the individual parking
%                                  zones
%                                  Size: Nx2
global draw;

%%connected components method to separate parking places
parkingPlaces=bwmorph(parkingPlaces,'open');
se =strel('line',11,90);
parkingPlaces=imerode(parkingPlaces,se);
cc = bwconncomp(parkingPlaces);

if draw==1
    labeled = labelmatrix(cc);
    RGB_label = label2rgb(labeled, @spring, 'c', 'shuffle');
    figure
    imshow(RGB_label,'InitialMagnification','fit')
end


parkingPlacesFound=[];
centerPointsOfZones=[];
for k=1:cc.NumObjects
    
    object = false(size(parkingPlaces));
    object(cc.PixelIdxList{k}) = true;
    object=imcomplement(object);
    
    s  = regionprops(~object,'Centroid');
    centerPointsOfZones=[centerPointsOfZones;s.Centroid];
end

%%Finding all points of skeleton thate are close to the centroids

parkingZoneCoordinates=[];
[row col]=find(skeletonOfParkingLot==1);
skeletonPoints=[col row];
for i=1:size(centerPointsOfZones,1)
    distances = sqrt(sum(bsxfun(@minus, skeletonPoints, centerPointsOfZones(i,:)).^2,2));
    %find the smallest distance and use that as an index into B:
    closest = skeletonPoints(find(distances==min(distances)),:);
    parkingZoneCoordinates(end+1,:)=closest(1,:);
end


%%creating the sequence of path by going to the closest point
parkingZoneCoordinatesInSequence = findSequenceOfPathCoordinates(parkingZoneCoordinates,startPoint);




%%%drawing lines between the closest points
if draw==1
    figure();
    imshow(~skeletonOfParkingLot)
    hold on
    plot(centerPointsOfZones(:,1),centerPointsOfZones(:,2),'b*','MarkerSize',20,'LineWidth',2);
    hold on
    plot(parkingZoneCoordinates(:,1),parkingZoneCoordinates(:,2),'r*','MarkerSize',20,'LineWidth',2);
    
    
    for i=1:size(parkingZoneCoordinatesInSequence)-1
        hold on
        plot([parkingZoneCoordinatesInSequence(i,1),parkingZoneCoordinatesInSequence(i+1,1)]...
            ,[parkingZoneCoordinatesInSequence(i,2),parkingZoneCoordinatesInSequence(i+1,2)],'LineWidth',5);
    end
end

if draw==1
    labeled = labelmatrix(cc);
    RGB_label = label2rgb(labeled, @spring, 'c', 'shuffle');
    figure
    imshow(RGB_label,'InitialMagnification','fit')
    hold on
    plot(centerPointsOfZones(:,1),centerPointsOfZones(:,2),'b*','MarkerSize',20,'LineWidth',1);
end

end

