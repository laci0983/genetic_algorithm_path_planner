%This program performs the optimization of the Voronoi diagram based
%traversal based on a genetic algorithm, where the goal is to optimize the
%Hamiltonian path problem
clear all
close all
clc

global startPoint;
global genNum;
global trendOfError;
global toggle;
toggle=1;
trendOfError=[];




testData=2;  %1:"Savoya", 2:"BME", 3:"trap"

if(testData==2)
    nodeStruct=load('BME_nodeVector.mat');
    nodesToReach=load('BME_isPointToReach.mat');
    skelstruct=load('BME_skeletonOfParkingLot.mat');
    skeletonOfParkingLot=skelstruct.skeletonOfParkingLot;
    graphStruct=load('BME_G.mat');
elseif(testData==1)
    nodeStruct=load('Savoya_nodeVector.mat');
    nodesToReach=load('Savoya_isPointToReach.mat');
    skelstruct=load('Savoya_skeletonOfParkingLot.mat');
    skeletonOfParkingLot=skelstruct.skeletonOfParkingLot;
    graphStruct=load('Savoya_G.mat');
elseif(testData==3)
    nodeStruct=load('trap_nodeVector.mat');
    nodesToReach=load('trap_isPointToReach.mat');
    skelstruct=load('trap_skeletonOfParkingLot.mat');
    skeletonOfParkingLot=skelstruct.skeletonOfParkingLot;
    graphStruct=load('trap_G.mat');    
end

extracted_graph=graphStruct.G;
nodeVector=nodeStruct.nodeVector;
isPointToReach=nodesToReach.isPointToReach;







genNum=1; %generation counter

Nind=10;
maxgen=100;
ggap=0.8;
mutpr=0.01;
rrate=0.4;

startPoint=1;
global G;
G=extracted_graph;

 Nvar=size(G.Nodes,1);



points=1:Nvar;

initpop=[];

for i=1:Nind
    idx=randperm(Nvar);
    condition=isPointToReach(idx)==0 | (points(idx)==startPoint)';
    idx(condition)=[];
    initpop(end+1,:)=points(idx);
end




% execute the genetic algorithm
fpop=mysga(initpop,maxgen,'objfn',ggap,mutpr,rrate);


%get the best individual after maxgen generations
objvals=objfn(fpop);
[M,I]=min(objvals);

bestRoute=fpop(I,:);
nodesInOrder=bestRoute(1);

for i=1:size(bestRoute,2)-2
    start=nodesInOrder(end);
    pointToReach=bestRoute(i+1);
    [I,~]=find(nodesInOrder==pointToReach);
    if(length(I)<1)
        %start=bestRoute(i);
        [nodes,~]=shortestpath(G,start,pointToReach);
        nodesInOrder=[nodesInOrder nodes(2:end)];
    end
end
[nodesStart,~]=shortestpath(G,startPoint,nodesInOrder(1));
[nodesEnd,~]=shortestpath(G,nodesInOrder(end),startPoint);
nodesInOrder=[nodesStart(1:end-1) nodesInOrder nodesEnd(2:end)];
figure; %plot(G)
imshow(skeletonOfParkingLot);
hold on
p=plot(G,'XData',nodeVector(:,1),'YData',nodeVector(:,2));
p.Marker='s';
p.NodeColor='r';
p.MarkerSize = 7;
p.EdgeColor='green';
p.LineWidth=4;
nodesInOrder
size(nodesInOrder,2)

figure
plot(trendOfError)
title('Trend of error');
xlabel('Generation')
ylabel('Objective function value')

path=nodeVector(nodesInOrder,:);
figure
imshow(skeletonOfParkingLot);
hold on;
plot(path(:,1),path(:,2),'Color','Green','LineWidth',4);
plot(nodeVector(:,1),nodeVector(:,2),'rx');


figure;
imshow(skeletonOfParkingLot);
hold on;
plot(path(:,1),path(:,2),'Color','Green','LineWidth',4);
nodePlot=[nodeVector zeros(size(nodeVector,1),1)];
for i=1:size(nodesInOrder,2)
    idx=nodesInOrder(i);
    x=nodeVector(idx,1);
    y=nodeVector(idx,2);
    offset=nodePlot(idx,3);
    plot(x,y,'bx','Markersize',20,'LineWidth',5);
    text(x-offset,y,[num2str(i),','],'FontSize',20,'Color','r');
    nodePlot(idx,3)=nodePlot(idx,3)-10;
end
