function new_population=reinsert(population,offspring,rrate)
% The funciton performs the reinsertion of offspring with a specific rate
% of reinsertion.
%@param:
%        population  - original population
%        offspring   - new offsprings
%        rrate       - reinsertion rate
%@return:
%       new_population - population with reinserted offsprings

new_population=([population(1:rrate,:); 
offspring(1:size(population,1)-rrate,:)]);

